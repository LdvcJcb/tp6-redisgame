window.addEventListener("load", async () => {
    // DOM elements
    let btnLettre = document.getElementById("btnLettre"),
        btnMot = document.getElementById("btnMot"),
        btnPseudo = document.getElementById("btnPseudo"),
        essais = document.getElementById("essais"),
        joueur = document.getElementById("joueur"),
        joueurs = document.getElementById("joueurs"),
        lettre = document.getElementById("lettre"),
        mot = document.getElementById("mot"),
        motATrouver = document.getElementById("motATrouver"),
        points = document.getElementById("points"),
        propositions = document.getElementById("propositions"),
        pseudoBlock = document.getElementById("pseudoBlock"),
        ttl = document.getElementById("ttl"),
        user = document.getElementById("user");

    // Time left interval
    let timer;

    // Base URL
    let url = "http://localhost:8080/api";

    // Get word to find
    let response  = await fetch(url + "/mot");
    let data = await response.json();

    // Generate underscores for the game according to already proposed letters
    if (data.code === 200) {
        let word = data.contenu.mot;
        let propsFetch = await fetch(url + "/propositions");
        let propsData = await propsFetch.json();
        motATrouver.textContent = Array.from(word).reduce((acc, val, idx) => {
            let space = (idx !== 0) ? " " : "";
            return acc + space + "_";
        }, "");
        
        if (propsData.contenu.length > 0) {
            // Add elements to propositions list
            propositions.textContent = "";
            propsData.contenu.forEach(e => {
                let li = document.createElement("li");
                li.textContent = e;
                propositions.prepend(li);
            });

            motATrouver.textContent = Array.from(word).reduce((acc, val, idx) => {
                // Set the accumulator
                let space = (idx !== 0) ? " " : "";
                if (propsData.contenu.includes(val)) {
                    return acc + space + val;
                }
                return acc + space + "_";
            }, "");
        }

        // Number of tries left
        let str = (data.contenu.nbessais <= 1) ? " essai" : " essais"; 
        essais.textContent = data.contenu.nbessais + str;

        // Get TTL
        let ttlFetch = await fetch(url + "/ttl");
        let ttlData = await ttlFetch.json();
        let seconds = parseInt(ttlData.contenu, 10);
        let ttlTxt = (seconds <= 1) ? " seconde" : " secondes";
        ttl.textContent = seconds + ttlTxt;

        // Interval for the timer
        timer = setInterval(async () => {
            let left = parseInt(ttl.textContent, 10) - 1;
            let txt = (left <= 1) ? " seconde" : " secondes";
            ttl.textContent = left + txt;

            // Clean up when the timer is done
            if (left === 0) {
                // Reset
                // Set points
                let formData = new FormData();
                formData.append("joueur", sessionStorage.getItem("id"));
                formData.append("isWinner", true);
                formData.append("nbessais", parseInt(essais.textContent, 10));
                let pointsFetch = await fetch(url + "/points", { method: "POST", body: formData });
                await pointsFetch.json();

                // Get usersession point
                formData = new FormData();
                formData.append("id", sessionStorage.getItem("id"));
                pointsFetch = await fetch(url + "/userpoints", { method: "POST", body: formData });
                let pointsData = await pointsFetch.json();
                points.textContent = pointsData.contenu;

                // Empty all
                let resetFetch = await fetch(url + "/reset", { method: "POST" });
                let resetData = await resetFetch.json();
                if (resetData.code === 200) {
                    propositions.textContent = "";
                    essais.textContent = "";
                }

                // Get no motATrouver message
                let motFetch  = await fetch(url + "/mot");
                let motData = await motFetch.json();
                motATrouver.textContent = motData.contenu;
                ttl.textContent = "";
                clearInterval(timer);
            }
        }, 1000);
    } else {
        motATrouver.textContent = data.contenu; // No motATrouver message
    }

    // Submit letter
    btnLettre.addEventListener("click", async () => {
        if (lettre.value) {
            let formData = new FormData();
            formData.append("lettre", lettre.value);
            let response = await fetch(url + "/lettre", { method: "POST", body: formData });
            let data = await response.json();
            if (data.code === 200) {
                // Mot
                let motFetch = await fetch(url + "/mot");
                let motData = await motFetch.json();

                // nbessais
                let tries = data.contenu.nbessais;
                let tryTxt = (data.contenu.nbessais <= 1) ? " essai" : " essais";
                essais.textContent = tries + tryTxt;

                let propsFetch = await fetch(url + "/propositions");
                let propsData = await propsFetch.json();

                // Add elements to propositions list
                propositions.textContent = "";
                propsData.contenu.forEach(e => {
                    let li = document.createElement("li");
                    li.textContent = e;
                    propositions.prepend(li);
                });

                motATrouver.textContent = Array.from(motData.contenu.mot).reduce((acc, val, idx) => {    
                    // Set the accumulator
                    let space = (idx !== 0) ? " " : "";
                    if (propsData.contenu.includes(val)) {
                        return acc + space + val;
                    }
                    return acc + space + "_";
                }, "");

                if (!motATrouver.textContent.includes("_") || tries === 0) { // round finished
                    // Reset
                    // Set points
                    let formData = new FormData();
                    formData.append("joueur", sessionStorage.getItem("id"));
                    formData.append("isWinner", false);
                    formData.append("nbessais", parseInt(essais.textContent, 10));
                    let pointsFetch = await fetch(url + "/points", { method: "POST", body: formData });
                    await pointsFetch.json();

                    // Get usersession point
                    formData = new FormData();
                    formData.append("id", sessionStorage.getItem("id"));
                    pointsFetch = await fetch(url + "/userpoints", { method: "POST", body: formData });
                    let pointsData = await pointsFetch.json();
                    points.textContent = pointsData.contenu;

                    // Empty all
                    let resetFetch = await fetch(url + "/reset", { method: "POST" });
                    let resetData = await resetFetch.json();
                    if (resetData.code === 200) {
                        propositions.textContent = "";
                        essais.textContent = "";
                    }

                    // Get no motATrouver message
                    let motFetch  = await fetch(url + "/mot");
                    let motData = await motFetch.json();
                    motATrouver.textContent = motData.contenu;
                    ttl.textContent = "";
                    clearInterval(timer);
                }
            }
        }
    });

    // Submit word
    btnMot.addEventListener("click", async () => {
        if (mot.value) {
            let formData = new FormData();
            formData.append("mot", mot.value);
            let response = await fetch(url + "/mot", { method: "POST", body: formData });
            let data = await response.json();

            // Start time left timer
            if (data.code === 200) {
                // Determine if submit is solution or not
                if (parseInt(essais.textContent, 10)) { // Check nbessais has a value or equals 0 => solution
                    if (data.contenu) { // not found
                        // Add to propositions
                        let li = document.createElement("li");
                        li.textContent = mot.value;
                        propositions.prepend(li);

                        // Set nbessais
                        let tries = data.contenu.nbessais;
                        let tryTxt = (tries <= 1) ? " essai" : " essais"
                        essais.textContent = tries + tryTxt;

                        if (tries === 0) {
                            // Reset
                            // Set points
                            let formData = new FormData();
                            formData.append("joueur", sessionStorage.getItem("id"));
                            formData.append("isWinner", false);
                            formData.append("nbessais", parseInt(essais.textContent, 10));
                            let pointsFetch = await fetch(url + "/points", { method: "POST", body: formData });
                            await pointsFetch.json();

                            // Get usersession point
                            formData = new FormData();
                            formData.append("id", sessionStorage.getItem("id"));
                            pointsFetch = await fetch(url + "/userpoints", { method: "POST", body: formData });
                            let pointsData = await pointsFetch.json();
                            points.textContent = pointsData.contenu;

                            // Empty all
                            let resetFetch = await fetch(url + "/reset", { method: "POST" });
                            let resetData = await resetFetch.json();
                            if (resetData.code === 200) {
                                propositions.textContent = "";
                                essais.textContent = "";
                            }

                            // Get no motATrouver message
                            let motFetch  = await fetch(url + "/mot");
                            let motData = await motFetch.json();
                            motATrouver.textContent = motData.contenu;
                            ttl.textContent = "";
                            clearInterval(timer);
                        }
                    } else { // found => reset
                        // Reset
                        // Set points
                        let formData = new FormData();
                        formData.append("joueur", sessionStorage.getItem("id"));
                        formData.append("isWinner", false);
                        formData.append("nbessais", parseInt(essais.textContent, 10));
                        let pointsFetch = await fetch(url + "/points", { method: "POST", body: formData });
                        await pointsFetch.json();

                        // Get usersession point
                        formData = new FormData();
                        formData.append("id", sessionStorage.getItem("id"));
                        pointsFetch = await fetch(url + "/userpoints", { method: "POST", body: formData });
                        let pointsData = await pointsFetch.json();
                        points.textContent = pointsData.contenu;

                        // Empty all
                        let resetFetch = await fetch(url + "/reset", { method: "POST" });
                        let resetData = await resetFetch.json();
                        if (resetData.code === 200) {
                            propositions.textContent = "";
                            essais.textContent = "";
                        }

                        // Get no motATrouver message
                        let motFetch  = await fetch(url + "/mot");
                        let motData = await motFetch.json();
                        motATrouver.textContent = motData.contenu;
                        ttl.textContent = "";
                        clearInterval(timer);
                    }
                } else { // new mot proposal
                    // Set joueur
                    let formData = new FormData();
                    formData.append("joueur", sessionStorage.getItem("nom"));
                    let joueurFetch = await fetch(url + "/joueur", { method: "POST", body: formData });
                    await joueurFetch.json();

                    // nbessais
                    let tries = data.contenu.nbessais;
                    let tryTxt = (tries <= 1) ? " essai" : " essais"
                    essais.textContent = tries + tryTxt;

                    // TTL
                    let seconds = data.contenu.ttl;
                    let secTxt = (seconds <= 1) ? " seconde" : " secondes"
                    ttl.textContent = seconds + secTxt;
                    
                    // Generate underscores
                    let txt = Array.from(mot.value).reduce((acc, val, idx) => {
                        let space = (idx !== 0) ? " " : "";
                        return acc + space + "_";
                    }, "");
                    motATrouver.textContent = txt;

                    // Interval for the timer
                    timer = setInterval(async () => {
                        let left = parseInt(ttl.textContent, 10) - 1;
                        let txt = (left <= 1) ? " seconde" : " secondes";
                        ttl.textContent = left + txt;

                        // Clean up when the timer is done
                        if (left === 0) {
                            // Reset
                            // Set points
                            let formData = new FormData();
                            formData.append("joueur", sessionStorage.getItem("id"));
                            formData.append("isWinner", true);
                            formData.append("nbessais", parseInt(essais.textContent, 10));
                            let pointsFetch = await fetch(url + "/points", { method: "POST", body: formData });
                            await pointsFetch.json();

                            // Get usersession point
                            formData = new FormData();
                            formData.append("id", sessionStorage.getItem("id"));
                            pointsFetch = await fetch(url + "/userpoints", { method: "POST", body: formData });
                            let pointsData = await pointsFetch.json();
                            points.textContent = pointsData.contenu;

                            // Empty all
                            let resetFetch = await fetch(url + "/reset", { method: "POST" });
                            let resetData = await resetFetch.json();
                            if (resetData.code === 200) {
                                propositions.textContent = "";
                                essais.textContent = "";
                            }

                            // Get no motATrouver message
                            let motFetch  = await fetch(url + "/mot");
                            let motData = await motFetch.json();
                            motATrouver.textContent = motData.contenu;
                            ttl.textContent = "";
                            clearInterval(timer);
                        }
                    }, 1000);
                }
            }
        }
    });

    //Submit pseudo
    btnPseudo.addEventListener("click", async () => {
        if (pseudo.value) {
            let formData = new FormData();
            formData.append("pseudo", pseudo.value);
            let response = await fetch(url + "/pseudo", { method: "POST", body: formData });
            let data = await response.json();
            if (data.code === 200) {
                // Add pseudo to player list
                joueurs.textContent = "";
                // Fetch joueurs
                let plsFetch = await fetch(url + "/joueurs");
                let plsData = await plsFetch.json();
                plsData.contenu.forEach(e => {
                    let li = document.createElement("li");
                    li.textContent = e.nom;
                    joueurs.prepend(li);
                });

                // Set header
                let sessionFetch = await fetch(url + "/session");
                let sessionData = await sessionFetch.json();
                joueur.textContent = sessionData.contenu.pseudo;
                points.textContent = sessionData.contenu.points;
                sessionStorage.setItem("id", sessionData.contenu.id);
                sessionStorage.setItem("nom", sessionData.contenu.pseudo);
                user.style.cssText = "display: block";

                // Hide block
                pseudoBlock.style.cssText = "display: none";
            }
        }
    });

    // Retrieve session
    let sessionFetch = await fetch(url + "/session");
    let sessionData = await sessionFetch.json();
    if (sessionData.code === 404) {
        user.style.cssText = "display: none";
        pseudoBlock.style.cssText = "display: block";
    } else {
        joueur.textContent = sessionData.contenu.pseudo;
        points.textContent = sessionData.contenu.points;
        sessionStorage.setItem("id", sessionData.contenu.id);
        sessionStorage.setItem("nom", sessionData.contenu.pseudo);
        user.style.cssText = "display: block";
        pseudoBlock.style.cssText = "display: none";
    }
                
    // Fetch joueurs
    let plsFetch = await fetch(url + "/joueurs");
    let plsData = await plsFetch.json();
    plsData.contenu.forEach(e => {
        let li = document.createElement("li");
        li.textContent = e.nom;
        joueurs.prepend(li);
    });
});

// TODO: check user's rights
// TODO: fetch when possible
// TODO: controls
// TODO: display points