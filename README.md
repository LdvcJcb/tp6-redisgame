# TP6-RedisGame

Repo git pour le pendu en ligne

## Membres du groupe

Chahine Boutaljante
Ludovic Jacob
Amir Khodja
Flavien Rizoulieres
Shiny Saing

## Pré-requis

Le projet s'appuie sur Redis et sur PHP. Il est possible de démarrer le serveur Redis à l'aide du fichier **docker-compose.yml** fourni dans le projet.

## Structure de la base de données Redis

```
count:joueurs: nombre de joueurs (auto incrément)
joueurs:{id}: hset de joueur: nom, points
mot: mot proposé par joueur
joueur: id du joueur qui a proposé mot
nbessais: nombre d'essais restants
propositions: set de lettres ou de mots (solutions) proposés
```

## Structure des réponses du serveur PHP

```
statut: statut de la réponse
code: code HTTP de la réponse
contenu (facultatif): donnée demandée en cas de succès ou le message d'erreur
```

## Usage

Pour lancer le projet, démarrez le conteneur Redis et le serveur PHP. Le pendu est disponible en ouvrant le fichier **page.html** avec le navigateur de votre choix.

```bash
docker-compose up -d
php -S localhost:8080
```

## Scalegrid

Au cas où le container Docker ne fonctionne pas (Connection refused par exemple), la configuration suivante peut être utilisée dans le fichier **index.php**:

````php
$redis = new Predis\Client(array(
    "scheme" => "tcp",
    "host" => "SG-redis-36220.servers.mongodirector.com",
    "port" => 6379,
    "password" => "0tvDUygGzfVnHsy9sQcVTHboSG4i8E0P"
));
```