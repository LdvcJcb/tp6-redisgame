<?php

require "predis/autoload.php";
Predis\Autoloader::register();

if (session_status() === PHP_SESSION_NONE){
    session_start();
}

// Retrieve Redis connector
function getConnector() {
    if (!isset($redis)) {
        try {
            $redis = new Predis\Client(array(
                "scheme" => "tcp",
                "host" => "SG-redis-36220.servers.mongodirector.com",
                "port" => 6379,
                "password" => "0tvDUygGzfVnHsy9sQcVTHboSG4i8E0P"
            ));
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }
    return $redis;
}

// Generic function to ease the creation of response
function setResponse($status, $code, $content = NULL) {
    $response = array();
    $response['statut'] = $status;
    $response['code'] = $code;
    if (isset($content)) {
        $response['contenu'] = $content;
    }
    return $response;
}

// Fetch the word in the DB
function getWord() {
    // Check there's a word
    if (!getConnector()->exists("mot")) {
        $response = setResponse("erreur", 400, "En attente d'un mot à trouver");
    } else {
        // Actually fetch it
        $word = array();
        $word['mot'] = getConnector()->get("mot");
        $word['nbessais'] = getConnector()->get("nbessais");
        $response = setResponse("succes", 200, $word);
    }
    return json_encode($response);
}

// Set the word in the DB
function setWord($payload) {
    // Check payload
    if (!isset($payload['mot'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        // Set the key with an expiration of 60 seconds
        getConnector()->set("mot", $payload['mot']);
        getConnector()->set("nbessais", 10);
        getConnector()->expire("mot", 60);
        $word = array(
            "value" => $payload['mot'],
            "nbessais" => 10,
            "ttl" => 60,
        );
        $response = setResponse("succes", 200, $word);
    }
    return json_encode($response);
}

// Add player to list
function addPlayer($payload) {
    if (!isset($payload['pseudo'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        // Save the player
        $count = getConnector()->get("count:joueurs");
        getConnector()->hset("joueurs:" . ++$count, "nom", $payload['pseudo']);
        getConnector()->hset("joueurs:" . $count, "points", 0);
        getConnector()->set("count:joueurs", $count);
        $response = setResponse("success", 200, array("pseudo" => $payload['pseudo'], "points" => 0, "id" => "joueurs:" . $count));
        $_SESSION['id'] = "joueurs:" . $count;
    }
    return json_encode($response);
}

// Retrieve players
function getPlayers() {
    $keys = getConnector()->keys("joueurs:*");
    $players = array();
    foreach ($keys as $key) {
        $data = getConnector()->hgetall($key);
        array_push($players, $data);
    }
    return json_encode(setResponse("succes", 200, $players));
}

// Retrieve the word's TTL from the DB
function getTTL($key) {
    $ttl = getConnector()->ttl($key);
    return json_encode(setResponse("succes", 200, $ttl));
}

// Add letter to set of propositions in the DB
function proposeLetter($payload) {
    if (!isset($payload['lettre'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        // TODO: case all letters have been found -> atm done during reset
        $word = getConnector()->get("mot");
        $tries = getConnector()->get("nbessais");
        
        // Check letter in word
        if (!strstr($payload['lettre'], $word)) {
            // Set nbessais-- value
            getConnector()->set("nbessais", --$tries);
        }
        // Push letter to propositions
        getConnector()->sadd("propositions", $payload['lettre']);

        // Response
        $res = array("valeur" => $word, "nbessais" => $tries);
        return json_encode(setResponse("succes", 200, $res));
    }
}

// Add word to the set of propositions in the DB
function proposeWord($payload) {
    if (!isset($payload['mot'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        // Determine if proposed mot is a solution or motATrouver
        if (getConnector()->exists("mot")) {
            $word = getConnector()->get("mot");
            $tries = getConnector()->get("nbessais");

            // Compare mot in DB to submitted word
            if (strcmp($word, $payload['mot']) == 0) {
                getConnector()->del("mot");
                $res = null;
            } else {
                // Push to propositions
                getConnector()->sadd("propositions", $payload['mot']);
    
                // Set nbessais value
                getConnector()->set("nbessais", --$tries);  
                $res = array("valeur" => $word, "nbessais" => $tries);          
            }
            
            // Response
            return json_encode(setResponse("succes", 200, $res));
        }
        // Set mot in DB
        return setWord($payload);
    }
}

// Retrieve propositions from the DB
function getPropositions() {
    $props = getConnector()->smembers("propositions");
    // Return empty array if there's no propositions
    if ($props == false) {
        $props = array();
    }
    return json_encode(setResponse("succes", 200, $props));
}

// Retrieve number of tries from the DB
function getNbTries() {
    $props = getConnector()->get("nbessais");
    return json_encode(setResponse("succes", 200, $props));
}

// Reset game
function resetContext() {
    getConnector()->del("mot");
    getConnector()->del("propositions");
    getConnector()->del("nbessais");
    return json_encode(setResponse("succes", 200));
}

// Set user session
function setUserSession($payload) {
    if ($payload['pseudo']) {
        $_SESSION['user'] = $payload['pseudo'];
        $_SESSION['points'] = 0;
        return addPlayer($payload);
    }
    return json_encode(setResponse("erreur", 400));
}

// Get user session
function getUserSession() {
    if (!empty($_SESSION['user']) && isset($_SESSION['points']) && isset($_SESSION['id'])) {
        return json_encode(setResponse("succes", 200, array(
            "pseudo" => $_SESSION['user'],
            "points" => $_SESSION['points'],
            "id" => $_SESSION['id']
        )));
    }
    return json_encode(setResponse("erreur", 404));
}

// Set joueur who proposed mot
function setJoueur($payload) {
    if (!isset($payload['joueur'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        getConnector()->set("joueur", $payload['joueur']);
        getConnector()->expire("joueur", 60);
        $response = setResponse("succes", 200);
    }
    return json_encode($response);
}

// Set points
function setPoints($payload) {
    if (!isset($payload['joueur']) || !isset($payload['isWinner']) || !isset($payload['nbessais'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        // Set points to winner
        if ($payload['isWinner']) { // winner is joueur
            $points = getConnector()->hget($payload['joueur'], "points");
            getConnector()->hset($payload['joueur'], "points", $points + 10);
            if (strcmp($_SESSION['id'], $payload['joueur']) == 0) {
                $_SESSION['points'] =  $points + 10;
            }
        } else { // winner is the group
            $keys = getConnector()->keys("joueurs:*");
            foreach ($keys as $key) {
                $points = getConnector()->hget($key, "points");
                getConnector()->hset($key, "points", $points + $payload['nbessais']);
            }
        }
        $response = setResponse("succes", 200);
    }
    return json_encode($response);
}

// Get points
function getPoints($payload) {
    if (!isset($payload['id'])) {
        $response = setResponse("erreur", 400, "La requête n'a pas pu être traité");
    } else {
        $points = getConnector()->hget($payload['id'], "points");
        $response = setResponse("succes", 200, $points);
    }
    return json_encode($response);
}

// // Clear session
// session_destroy();

// Parse the URL
if (isset($_SERVER['QUERY_STRING'])) {
    $params = $_SERVER['QUERY_STRING'];
} else {
    $params = $_SERVER['REQUEST_URI'];
}
$args = explode("/", $params);

if (!getConnector()->exists("count:joueurs")) {
    getConnector()->set("count:joueurs", 0);
}

// Basic routing system
switch ($_SERVER['REQUEST_METHOD']) {
    // GET routes
    case "GET":
        /* /propositions */
        if (in_array("propositions", $args)) {
            echo getPropositions();
            exit(0);
        }
        /* /mot */
        if (in_array("mot", $args)) {
            if (in_array("essais", $args)) {
                echo getNbTries();
            } else {
                echo getWord();
            }
            exit(0);
        }
        /* /ttl */
        if (in_array("ttl", $args)) {
            echo getTTL("mot");
            exit(0);
        }
        /* /session */
        if (in_array("session", $args)) {
            echo getUserSession();
            exit(0);
        }
        /* /joueurs */
        if (in_array("joueurs", $args)) {
            echo getPlayers();
            exit(0);
        }
        exit(1);

    // POST routes
    case "POST":
        /* /lettre */
        if (in_array("lettre", $args)) {
            echo proposeLetter($_POST);
            exit(0);
        }
        /* /mot */
        if (in_array("mot", $args)) {
            echo proposeWord($_POST);
            exit(0);
        }
        /* /reset */
        if (in_array("reset", $args)) {
            echo resetContext();
            exit(0);
        }
        /* /pseudo */
        if(in_array("pseudo", $args)) {
            echo setUserSession($_POST);
            exit(0);
        }
        /* /joueur */
        if(in_array("joueur", $args)) {
            echo setJoueur($_POST);
            exit(0);
        }
        /* /points */
        if(in_array("points", $args)) {
            echo setPoints($_POST);
            exit(0);
        }
        /* /userpoints */
        if (in_array("userpoints", $args)) {
            echo getPoints($_POST);
            exit(0);
        }
        exit(1);

    //  Error case
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit(1);
}

// TODO: account list
